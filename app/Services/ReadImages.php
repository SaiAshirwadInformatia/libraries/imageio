<?php

namespace App\Services;

class ReadImages
{
    public static function scan($directory, $ext = 'jpg')
    {
        $files = [];
        $input = array_slice(scandir($directory), 2);
        foreach ($input as $i) {
            if (is_dir($directory . $i)) {
                $files = array_merge($files, self::scan($directory . $i . DIRECTORY_SEPARATOR, $ext));
            } else if (str_ends_with($i, $ext)) {
                $files[] = $directory . $i;
            }
        }
        return $files;
    }
}
