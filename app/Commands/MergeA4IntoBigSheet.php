<?php

namespace App\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;
use Spatie\PdfToImage\Pdf;

class MergeA4IntoBigSheet extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'merge:a4-into-big-sheet
    {--once : For testing run only for first iteration}
    {--directory= : Directory containing all images}
    {--inner_width=3540 : Image width of input image}
    {--inner_height=2360 : Image width of input image}
    {--output_width=28800 : Output PDF width}
    {--output_height=14400 : Output PDF height}
    {--output= : Output Directory}';

    private array $cache = [];

    private string $directory = '';
    private bool $once = false;

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Merge 2 x A4 in single 12 x 18 page as PDF';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Started ImageIO for Merging A4 into 4ft x 8ft");
        $this->directory = $directory = $this->option('directory');
        $this->once = $once = $this->option('once');

        if (!file_exists($this->directory)) {
            throw new Exception("Invalid directory path: $this->directory");
        }

        $inner_width = $this->option('inner_width');
        $inner_height = $this->option('inner_height');
        $this->info("Inner widht: $inner_width height: $inner_height");
        $output_width = $this->option('output_width');
        $output_height = $this->option('output_height');
        $output = $this->option('output');

        $files = File::allFiles($directory);

        $bar = $this->output->createProgressBar(count($files));
        $bar->start();
        $merged = 0;

        $this->cache = [];
        if (file_exists($this->directory . "cache.json")) {
            $json = file_get_contents($this->directory . "cache.json");
            if (!empty($json)) {
                try {
                    $this->cache = json_decode($json, false, 512, JSON_THROW_ON_ERROR);
                } catch (\JsonException $e) {
                }
            }
        }
        try {

            collect($files)->chunk(44)->each(function ($jpgs, $index) use ($bar, $merged, $inner_width, $inner_height, $output_width, $output_height, $output) {
                $merged++;
                $newPdfPath = $output . DIRECTORY_SEPARATOR . str_pad($index + 1, 4, '0', STR_PAD_LEFT) . '.pdf';
                // $this->info("Processing batch: " . count($jpgs));

                $newPdf = new \TCPDF(
                    orientation: $output_width > $output_height ? 'L' : 'P',
                    unit: 'mm',
                    format: [(($output_width * 25.4) / 300), (($output_height * 25.4) / 300)],
                    unicode: true,
                    encoding: 'UTF-8',
                    diskcache: false
                );

                $newPdf->setCreator('Sai Ashirwad Informatia');
                $newPdf->setAuthor('saiashirwad.com');
                $newPdf->setTitle($newPdfPath);
                $newPdf->setMargins(0, 0, 0);
                $newPdf->setPrintHeader(false);
                $newPdf->SetHeaderMargin(0);
                $newPdf->SetFooterMargin(0);
                $newPdf->setPrintFooter(false);
                $newPdf->SetAutoPageBreak(false, 0);
                $newPdf->setImageScale(1);
                $newPdf->AddPage();

                $images = 0;
                $x = 360;

                $sub_jpgs_set = collect($jpgs)->chunk(6);
                foreach($sub_jpgs_set as $sub_jpgs) {
                    $y = 175;
                    // $this->info("Adding Row: " . count($sub_jpgs));
                    foreach ($sub_jpgs as $file) {
                        /** @var $file \SplFileInfo */
                        $newPdf->Image(
                            file: $file->getRealPath(),
                            x: ($x * 25.4 / 300),
                            y: ($y * 25.4 / 300),
                            w: ($inner_width * 25.4 / 300),
                            h: ($inner_height * 25.4 / 300),
                            type: '',
                            link: '',
                            align: '',
                            resize: true,
                            dpi: 300,
                            palign: '',
                            ismask: false,
                            imgmask: false,
                            border: 1,
                            fitonpage: false
                        );
                        $y = $y + $inner_height + 1;

                        $bar->advance();
                    }

                    $x = $x + $inner_width + 1;

                }


                $newPdf->Output($newPdfPath, 'F');
                $this->cache[] = $newPdfPath;
                $newPdf = null;
                $newPdfPath = null;
                if ($this->once) {
                    exit(0);
                }
            });
            $bar->finish();
            $this->saveCache();
            $this->info("Resized $merged images");
        } catch (\Exception $e) {
            $this->saveCache();
            $this->error("Failed: " . $e->getMessage());
        }
    }


    private function saveCache()
    {
        $this->warn("Creating cache");
        file_put_contents($this->directory . "cache.json", json_encode($this->cache));
        $this->info("Cache created");
        exec('find /tmp -maxdepth 1 -type f -name "magick-*" -delete');
    }

    /**
     * Get the list of signals handled by the command.
     *
     * @return array
     */
    public function getSubscribedSignals(): array
    {
        return [SIGINT, SIGTERM];
    }

    /**
     * Handle an incoming signal.
     *
     * @param int $signal
     * @return void
     */
    public function handleSignal(int $signal): void
    {
        if ($signal === SIGINT || $signal == SIGTERM) {

            $this->saveCache();
            exit(0);
        }
    }
}
