<?php

namespace App\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;
use Spatie\PdfToImage\Pdf;

class MergeA4IntoA3 extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'merge:a4-into-a3
    {--directory= : Directory containing all images}
    {--inner_width=2480 : Image width of input image}
    {--output_width=5400 : Output PDF width}
    {--output_height=3600 : Output PDF height}
    {--output= : Output Directory}';

    private $cache = [];

    private $directory = '';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Merge 2 x A4 in single 12 x 18 page as PDF';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Started ImageIO for Merging A4 into A3");
        $this->directory = $directory = $this->option('directory');

        if (!file_exists($this->directory)) {
            throw new Exception("Invalid directory path: $this->directory");
        }

        $inner_width = $this->option('inner_width');
        $output_width = $this->option('output_width');
        $output_height = $this->option('output_height');
        $output = $this->option('output');

        $files = File::allFiles($directory);

        $bar = $this->output->createProgressBar(count($files));
        $bar->start();
        $merged = 0;

        $this->cache = [];
        if (file_exists($this->directory . "cache.json")) {
            $json = file_get_contents($this->directory . "cache.json");
            if (!empty($json)) {
                $this->cache = json_decode($json);
            }
        }
        try {

            collect($files)->chunk(2)->each(function ($pdfs, $index) use ($bar, $merged, $inner_width, $output_width, $output_height, $output) {
                $files = [];
                $merged++;
                $newPdfPath = $output . DIRECTORY_SEPARATOR . str_pad($index + 1, 4, '0', STR_PAD_LEFT) . '.pdf';
                if (in_array($newPdfPath, $this->cache)) {
                    foreach ($pdfs as $pdf) {
                        $bar->advance();
                    }
                    return;
                }
                $newPdf = new \TCPDF(
                    orientation: $output_width > $output_height ? 'L' : 'P',
                    unit: 'mm',
                    format: [(($output_width * 25.4) / 300), (($output_height * 25.4) / 300)],
                    unicode: true,
                    encoding: 'UTF-8',
                    diskcache: false
                );

                $newPdf->setCreator('Sai Ashirwad Informatia');
                $newPdf->setAuthor('max-lab-qr-dashboard.saiashirwad.com');
                $newPdf->setTitle($newPdfPath);
                $newPdf->setMargins(0, 0, 0);
                $newPdf->setPrintHeader(false);
                $newPdf->SetHeaderMargin(0);
                $newPdf->SetFooterMargin(0);
                $newPdf->setPrintFooter(false);
                $newPdf->SetAutoPageBreak(false, 0);
                $newPdf->setImageScale(1);
                $newPdf->AddPage();

                $images = 0;

                foreach ($pdfs as $pdf) {
                    $file = $output . DIRECTORY_SEPARATOR . Str::random() . '.jpg';

                    $pdfImage = new Pdf($pdf->getPathname());
                    $pdfImage->width($inner_width)
                        ->setCompressionQuality(100)
                        ->setOutputFormat('jpg')
                        ->saveImage($file);
                    $pdfImage = null;
                    $files[] = $file;
                    $newPdf->Image(
                        file: $file,
                        x: (($inner_width * 25.4 / 300) * $images) + (($images == 0 ? 300 : 310) * 25.4 / 300),
                        y: 45 * 25.4 / 300,
                        w: ($inner_width * 25.4 / 300),
                        h: 0,
                        type: '',
                        link: '',
                        align: '',
                        resize: true,
                        dpi: 300,
                        palign: '',
                        ismask: false,
                        imgmask: false,
                        border: 0,
                        fitonpage: false
                    );

                    $images++;

                    $bar->advance();
                }

                $newPdf->Output($newPdfPath, 'F');
                $this->cache[] = $newPdfPath;
                $newPdf = null;

                foreach ($files as $file) {
                    unlink($file);
                }
                $files = null;
                $newPdfPath = null;

            });
            $bar->finish();
            $this->saveCache();
            $this->info("Resized $merged images");
        } catch (\Exception $e) {
            $this->saveCache();
            $this->error("Failed: " . $e->getMessage());
        }
    }


    private function saveCache()
    {
        $this->warn("Creating cache");
        file_put_contents($this->directory . "cache.json", json_encode($this->cache));
        $this->info("Cache created");
        exec('find /tmp -maxdepth 1 -type f -name "magick-*" -delete');
    }

    /**
     * Get the list of signals handled by the command.
     *
     * @return array
     */
    public function getSubscribedSignals(): array
    {
        return [SIGINT, SIGTERM];
    }

    /**
     * Handle an incoming signal.
     *
     * @param int $signal
     * @return void
     */
    public function handleSignal(int $signal): void
    {
        if ($signal === SIGINT || $signal == SIGTERM) {

            $this->saveCache();
            exit(0);
        }
    }
}
