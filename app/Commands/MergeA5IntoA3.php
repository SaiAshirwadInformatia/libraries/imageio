<?php

namespace App\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;
use Spatie\PdfToImage\Pdf;

class MergeA5IntoA3 extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'merge:a5-into-a3
    {--once : For testing run only for first iteration}
    {--directory= : Directory containing all images}
    {--inner_width=1750 : Image width of input image}
    {--inner_height=2479 : Image height of input image}
    {--output_width=3600 : Output PDF width}
    {--output_height=5400 : Output PDF height}
    {--output= : Output Directory}';

    private $cache = [];

    private $directory = '';
    private bool $once = false;

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Started ImageIO for Merging A5 into A3");
        $this->directory = $directory = $this->option('directory');
        $this->once = $once = $this->option('once');

        if (!file_exists($this->directory)) {
            throw new Exception("Invalid directory path: $this->directory");
        }

        $inner_width = $this->option('inner_width');
        $inner_height = $this->option('inner_height');
        $output_width = $this->option('output_width');
        $output_height = $this->option('output_height');

        $output = $this->option('output');

        $files = File::allFiles($directory);

        $bar = $this->output->createProgressBar(count($files));
        $bar->start();
        $merged = 0;

        $this->cache = [];
        if (file_exists($this->directory . "cache.json")) {
            $json = file_get_contents($this->directory . "cache.json");
            if (!empty($json)) {
                $this->cache = json_decode($json);
            }
        }
        try {


            $stateFiles = collect($files)->groupBy(function (\SplFileInfo $file, $key) {
                $basePath = '/Users/Shared/Workspace/Printing/Zuventus Momento/18thNov/';
                $path = str_replace($basePath, '', $file->getPathname());

                return substr($path, 0, strpos($path, DIRECTORY_SEPARATOR));
            });
            foreach ($stateFiles as $stateName => $files) {

                $files->chunk(4)->each(function ($pdfs, $index) use ($stateName, $merged, $bar, $inner_width, $inner_height, $output_width, $output_height, $output) {

                    $files = [];
                    $merged++;
                    $newPdfPath = $output . DIRECTORY_SEPARATOR . $stateName . DIRECTORY_SEPARATOR . str_pad($index + 1, 4, '0', STR_PAD_LEFT) . '.pdf';
                    if (!file_exists($output . DIRECTORY_SEPARATOR . $stateName)) {
                        mkdir($output . DIRECTORY_SEPARATOR . $stateName);
                    }
                    if (in_array($newPdfPath, $this->cache)) {
                        foreach ($pdfs as $pdf) {
                            // $bar->advance();
                        }
                        return;
                    }
                    $newPdf = new \TCPDF(
                        orientation: $output_width > $output_height ? 'L' : 'P',
                        unit: 'mm',
                        format: [(($output_width * 25.4) / 300), (($output_height * 25.4) / 300)],
                        unicode: true,
                        encoding: 'UTF-8',
                        diskcache: false
                    );

                    $newPdf->setCreator('Sai Ashirwad Informatia');
                    $newPdf->setAuthor('max-lab-qr-dashboard.saiashirwad.com');
                    $newPdf->setTitle($newPdfPath);
                    $newPdf->setMargins(0, 0, 0);
                    $newPdf->setPrintHeader(false);
                    $newPdf->SetHeaderMargin(0);
                    $newPdf->SetFooterMargin(0);
                    $newPdf->setPrintFooter(false);
                    $newPdf->SetAutoPageBreak(false, 0);
                    $newPdf->setImageScale(1);
                    $newPdf->AddPage();

                    $images = 0;

                    /**
                     * @var int $index
                     * @var \SplFileInfo $pdf
                     */
                    foreach ($pdfs as $index => $pdf) {
                        $file = $output . DIRECTORY_SEPARATOR . Str::random() . '.jpg';

                        if (str_ends_with($pdf->getBasename(), '.pdf')) {
                            $pdfImage = new Pdf($pdf->getPathname());
                            $pdfImage->width($inner_width)
                                ->setCompressionQuality(100)
                                ->setOutputFormat('jpg')
                                ->saveImage($file);
                            $pdfImage = null;
                        } else {
                            $file = $pdf->getPathname();
                        }
                        $x = 0;
                        $y = 0;
                        switch ($images) {
                            case 0:
                                $x = 45 * 25.4 / 300;
                                $y = 45 * 25.4 / 300;
                                break;
                            case 1:
                                $x = (50 + $inner_width) * 25.4 / 300;
                                $y = 45 * 25.4 / 300;
                                break;
                            case 2:
                                $x = 45 * 25.4 / 300;
                                $y = (90 + $inner_height) * 25.4 / 300;
                                break;
                            case 3;
                                $x = (50 + $inner_width) * 25.4 / 300;
                                $y = (90 + $inner_height) * 25.4 / 300;
                                break;
                        }
                        $files[] = $file;
                        $newPdf->Image(
                            file: $file,
                            x: $x,
                            y: $y,
                            w: ($inner_width * 25.4 / 300),
                            h: 0,
                            type: '',
                            link: '',
                            align: '',
                            resize: true,
                            dpi: 300,
                            palign: '',
                            ismask: false,
                            imgmask: false,
                            border: 0,
                            fitonpage: false
                        );

                        $images++;

                         $bar->advance();
                    }

                    $newPdf->Output($newPdfPath, 'F');
                    $this->cache[] = $newPdfPath;
                    $newPdf = null;

                    foreach ($files as $file) {
                        unlink($file);
                    }
                    $files = null;
                    $newPdfPath = null;

                });
            }
            $bar->finish();
            $this->saveCache();
            $this->info("Resized $merged images");
        } catch (\Exception $e) {
            $this->saveCache();
            $this->error("Failed: " . $e->getMessage());
        }
    }


    private function saveCache()
    {
        $this->warn("Creating cache");
        file_put_contents($this->directory . "cache.json", json_encode($this->cache));
        $this->info("Cache created");
        exec('find /tmp -maxdepth 1 -type f -name "magick-*" -delete');
    }

    /**
     * Get the list of signals handled by the command.
     *
     * @return array
     */
    public function getSubscribedSignals(): array
    {
        return [SIGINT, SIGTERM];
    }

    /**
     * Handle an incoming signal.
     *
     * @param int $signal
     * @return void
     */
    public function handleSignal(int $signal): void
    {
        if ($signal === SIGINT || $signal == SIGTERM) {

            $this->saveCache();
            exit(0);
        }
    }
}
