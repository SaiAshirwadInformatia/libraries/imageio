<?php

namespace App\Commands;

use App\Services\ReadImages;
use Illuminate\Console\Scheduling\Schedule;
use Intervention\Image\Facades\Image;
use LaravelZero\Framework\Commands\Command;
use Spatie\PdfToImage\Pdf;

class ResizeCanvasImageCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'bulk:resize-canvas
    {--once : Run this command only for one image for testing}
    {--directory= : Directory containing all images}
    {--ext=jpeg : Input extension to filter all images}
    {--output=jpg : Output extension}
    {--dpi=300 : Density of pixels per inch}
    {--x=0 : X-axis position from top-left}
    {--y=0 : X-axis position from top-left}
    {--width=3600 : Width of the photo to be resized into}
    {--height=2400 : Height of the photo to be resized into}
    {--inner-width=3510 : Resize inner width of image}
    {--inner-height=2340 : Resize inner height of image}
    {--border : Add a border surrounding the image }
    {--remove-pdf : Remove PDF}';

    private $cache = [];

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Resize image by maintaining the canvas dimensions';

    private $directory = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Started ImageIO for Bulk Image Canvas Resizing");
        $this->directory = rtrim($this->option('directory'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        $this->cache = [];
        if (file_exists($this->directory . "cache.json")) {
            $json = file_get_contents($this->directory . "cache.json");
            if (!empty($json)) {
                $this->cache = json_decode($json);
            }
        }
        $once = $this->option('once');
        $ext = $this->option('ext');
        $output = $this->option('output');
        $dpi = $this->option('dpi');
        $width = $this->option('width');
        $height = $this->option('height');
        $x = $this->option('x');
        $y = $this->option('y');
        $inner_width = $this->option('inner-width');
        $inner_height = $this->option('inner-height');
        $border = $this->option('border');
        $removePdf = $this->option('remove-pdf');

        if (!file_exists($this->directory)) {
            throw new Exception("Invalid directory path: $this->directory");
        }

        $files = ReadImages::scan($this->directory, $ext);

        $bar = $this->output->createProgressBar(count($files));
        $this->info("Identified: " . count($files) . " files of extension $ext");

        $bar->start();
        $resize = 0;

        foreach ($files as $file) {
            if (in_array($file, $this->cache)) {
                // $this->warn("Skipping from cache: $file");
                continue;
            }
            if (str_ends_with($file, 'pdf')) {
                $newFile = $file . '.' . $output;
                (new Pdf($file))
                    ->width($inner_width)
                    ->setCompressionQuality(100)
                    ->setOutputFormat($output)
                    ->saveImage($newFile);
                if ($removePdf) {
                    unlink($file);
                }
                $file = $newFile;
            }
            $temp = Image::make($file);
//            if ($temp->width() == $width && $temp->height() == $height) {
//                $temp->destroy();
//                $temp = null;
//                // $this->warn("Skipping: " . basename($file));
//                $this->cache[] = $file;
//                continue;
//            }
            // $this->warn("Resizing: $file");
            $image = Image::canvas($width, $height);
            $innerImage = Image::make($file)->resize($inner_width, $inner_height);
            $image->insert($innerImage, 'top-left', $x, $y);

            $finalImage = $image->getCore();
            $finalImage->setImageUnits(\imagick::RESOLUTION_PIXELSPERINCH);
            $finalImage->setImageResolution($dpi, $dpi);
            if ($border) {
                $image->rectangle($x -1, $y - 1, $inner_width + 1, $inner_height + 1, fn ($draw) => $draw->border(1, '#000000'));
            }
            $image->save(path: $file, quality: 100);
            $image = null;
            $this->cache[] = $file;
            // $this->info("Resize Completed: $file");
            $resize++;
            $bar->advance();
            if ($once) {
                $this->info("Resize completed: $file");
                exec("open '$file'");
                die();
            }
        }
        $bar->finish();
        $this->saveCache();
        $this->info("Resized $resize images");
    }

    private function saveCache()
    {
        $this->warn("Creating cache");
        file_put_contents($this->directory . "cache.json", json_encode($this->cache));
        $this->info("Cache created");
        if (file_exists('/tmp')) {
            exec('find /tmp -maxdepth 1 -type f -name "magick-*" -delete');
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
