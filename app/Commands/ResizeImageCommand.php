<?php

namespace App\Commands;

use App\Services\ReadImages;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Command\SignalableCommandInterface;

class ResizeImageCommand extends Command implements SignalableCommandInterface
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'bulk:resize
    {--directory= : Directory containing all images}
    {--ext=jpg : Input extension to filter all images}
    {--output=jpg : Output extension}
    {--dpi=300 : Density of pixels per inch}
    {--overwrite : Overwrite same image as that of input}
    {--width=2400 : Width of the photo to be resized into}
    {--height=3600 : Height of the photo to be resized into}
    {--border : Add border for resized image}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Resize images in bulk';

    private $cache;

    private $directory;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Started ImageIO for Bulk Image Resizing");
        $this->directory = rtrim($this->option('directory'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        $this->cache = [];
        if (file_exists($this->directory . "cache.json")) {
            $json = file_get_contents($this->directory . "cache.json");
            if (!empty($json)) {
                $this->cache = json_decode($json);
            }
        }
        $ext = $this->option('ext');
        $output = $this->option('output');
        $dpi = $this->option('dpi');
        $width = $this->option('width');
        $height = $this->option('height');
        $border = $this->option('border');

        if (!file_exists($this->directory)) {
            throw new Exception("Invalid directory path: $this->directory");
        }

        $files = ReadImages::scan($this->directory, $ext);

        $bar = $this->output->createProgressBar(count($files));
        $this->info("Identified: " . count($files) . " files of extension $ext");

        $bar->start();
        $resize = 0;
        foreach ($files as $file) {
            $bar->advance();
            if (in_array($file, $this->cache)) {
                // $this->warn("Skipping from cache: $file");
                continue;
            }
            $image = Image::make($file);
            $originalWidth = $image->width();
            $originalHeight = $image->height();
            if ($originalWidth == $width && $originalHeight == $height) {
                $image->destroy();
                $image = null;
                // $this->warn("Skipping: " . basename($file));
                $this->cache[] = $file;
                continue;
            }
            // $this->warn("Resizing: $file");
            $image->resizeCanvas($width, $height, 'top-left', false, '#ffffff');
            if ($border) {
                $image->rectangle(-1, -1, $originalWidth + 1, $originalHeight + 1, fn ($draw) => $draw->border(1, '#000000'));
            }
            $finalImage = $image->getCore();
            $finalImage->setImageUnits(\imagick::RESOLUTION_PIXELSPERINCH);
            $finalImage->setImageResolution($dpi, $dpi);
            $image->save(quality: 100);
            $this->cache[] = $file;
            // $this->info("Resize Completed: $file");
            $resize++;
        }
        $bar->finish();
        $this->saveCache();
        $this->info("Resized $resize images");
    }

    private function saveCache()
    {
        $this->warn("Creating cache");
        file_put_contents($this->directory . "cache.json", json_encode($this->cache));
        $this->info("Cache created");
        exec('find /tmp -maxdepth 1 -type f -name "magick-*" -delete');
    }

    /**
     * Get the list of signals handled by the command.
     *
     * @return array
     */
    public function getSubscribedSignals(): array
    {
        return [SIGINT, SIGTERM];
    }

    /**
     * Handle an incoming signal.
     *
     * @param  int  $signal
     * @return void
     */
    public function handleSignal(int $signal): void
    {
        if ($signal === SIGINT || $signal == SIGTERM) {

            $this->saveCache();
            exit(0);
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
