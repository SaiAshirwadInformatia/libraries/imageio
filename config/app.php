<?php return array (
  'name' => 'Imageio',
  'version' => '1.0.1',
  'env' => 'production',
  'providers' => 
  array (
    0 => 'App\\Providers\\AppServiceProvider',
    1 => 'Intervention\\Image\\ImageServiceProvider',
  ),
);
